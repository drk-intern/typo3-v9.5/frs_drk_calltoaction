<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

// register the icon for wizard usage
// do not use "use" for \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider due to TYPO3 caching bug!
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$iconRegistry->registerIcon(
    'frs_drk_calltoaction-ce-calltoaction',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    array(
        'source' => 'EXT:frs_drk_calltoaction/Resources/Public/Icons/20160113_DRK_Backend_Icons_Lesezeichen.svg'
    )
);

<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'FrsDrkCalltoaction',
    'Main',
    array(
        \Frs\FrsDrkCalltoaction\Controller\MainController::class => 'show',

    ),
    // non-cacheable actions
    array(
        \Frs\FrsDrkCalltoaction\Controller\MainController::class => '',

    )
);

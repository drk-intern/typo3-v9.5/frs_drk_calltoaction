plugin.tx_frsdrkcalltoaction {
    view {
        templateRootPaths.0 = {$plugin.tx_frsdrkcalltoaction.view.templateRootPath}
        partialRootPaths.0 = {$plugin.tx_frsdrkcalltoaction.view.partialRootPath}
        layoutRootPaths.0 = {$plugin.tx_frsdrkcalltoaction.view.layoutRootPath}
    }

    persistence {
        storagePid = {$plugin.tx_frsdrkcalltoaction.persistence.storagePid}
    }
}

tt_content {
    frs_drk_calltoaction = COA
    frs_drk_calltoaction {
        10 = FLUIDTEMPLATE
        10 {
            templateName = CallToAction
            layoutRootPaths.10 = {$plugin.tx_frsdrkcalltoaction.view.layoutRootPath}
            partialRootPaths.10 = {$plugin.tx_frsdrkcalltoaction.view.partialRootPath}
            templateRootPaths.10 = {$plugin.tx_frsdrkcalltoaction.view.templateRootPath}
            dataProcessing {
                10 = TYPO3\CMS\Frontend\DataProcessing\DatabaseQueryProcessor
                10 {
                    if.isTrue.field = tx_frsdrkcalltoaction_main
                    table = tx_frsdrkcalltoaction_domain_model_main
                    uidInList.field = tx_frsdrkcalltoaction_main
                    pidInList = 0
                    as = callToActionUid

                    dataProcessing {
                        10 = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
                        10 {
                            references.fieldName = image
                            as = files
                        }
                    }
                }
            }
        }

    }
}

lib.calltoaction.contentElementRendering = RECORDS
lib.calltoaction.contentElementRendering {
    tables = tx_frsdrkcalltoaction_domain_model_main
    source.current = 1
    dontCheckPid = 1
    conf.tx_frsdrkcalltoaction_domain_model_main = COA
    conf.tx_frsdrkcalltoaction_domain_model_main {
        10 = FLUIDTEMPLATE
        10 {
            templateName = CallToAction
            layoutRootPaths.10 = {$plugin.tx_frsdrkcalltoaction.view.layoutRootPath}
            partialRootPaths.10 = {$plugin.tx_frsdrkcalltoaction.view.partialRootPath}
            templateRootPaths.10 = {$plugin.tx_frsdrkcalltoaction.view.templateRootPath}
            dataProcessing {
                10 = TYPO3\CMS\Frontend\DataProcessing\DatabaseQueryProcessor
                10 {
                    table = tx_frsdrkcalltoaction_domain_model_main
                    uidInList.field = uid
                    pidInList = 0
                    as = callToActionUid

                    dataProcessing {
                        10 = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
                        10 {
                            references.fieldName = image
                            as = files
                        }
                    }
                }
            }
        }

    }
}

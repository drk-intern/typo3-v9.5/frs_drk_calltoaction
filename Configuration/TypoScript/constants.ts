plugin.tx_frsdrkcalltoaction {
    view {
        # cat=plugin.tx_frsdrkcalltoaction/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:frs_drk_calltoaction/Resources/Private/Templates/
        # cat=plugin.tx_frsdrkcalltoaction/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:frs_drk_calltoaction/Resources/Private/Partials/
        # cat=plugin.tx_frsdrkcalltoaction/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:frs_drk_calltoaction/Resources/Private/Layouts/
    }

    persistence {
        # cat=plugin.tx_frsdrkcalltoaction/a; type=string; label=Default storage PID
        storagePid =
    }
}

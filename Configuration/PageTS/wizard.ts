// configure a bookmark element.
mod.wizards.newContentElement.wizardItems.common {
    elements.frs_drk_calltoaction {
        iconIdentifier = frs_drk_calltoaction-ce-calltoaction
        title = LLL:EXT:frs_drk_calltoaction/Resources/Private/Language/locallang.xlf:wizard.title
        description = LLL:EXT:frs_drk_calltoaction/Resources/Private/Language/locallang.xlf:wizard.description
        tt_content_defValues {
            CType = frs_drk_calltoaction
        }
    }

    show := addToList(frs_drk_calltoaction)
}

<?php

$langFilePrefix = 'LLL:EXT:frs_drk_calltoaction/Resources/Private/Language/locallang_db.xlf:';

return array(
    'ctrl' => array(
        'title' => $langFilePrefix . 'tx_frsdrkcalltoaction_domain_model_main',
        'label' => 'row_description',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,

        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'type' => 'calltoactiontype',
        'enablecolumns' => array(
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ),
        'security' => [
            'ignorePageTypeRestriction' => true
        ],
        'searchFields' => 'title,row_description,',
        'iconfile' => 'EXT:frs_drk_calltoaction/Resources/Public/Icons/tx_frsdrkcalltoaction_domain_model_main.gif'
    ),
    'types' => array(
        // default bookmark
        '1' => array('showitem' => 'hidden,--palette--;;1,calltoactiontype,row_description,image,title,bodytext,--palette--,link,linktext,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,starttime,endtime,sys_language_uid'),
        // bookmark with amount
        '2' => array('showitem' => 'hidden,--palette--;;1,calltoactiontype,row_description,image,title,bodytext,--palette--,link,linktext,amount,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,starttime,endtime,sys_language_uid'),
        // bookmark with purpose and advertising code
        '3' => array('showitem' => 'hidden,--palette--;;1,calltoactiontype,row_description,image,title,bodytext,--palette--,link,linktext,purpose,advertising_code,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,starttime,endtime,sys_language_uid'),
        // bookmark with amount and purpose and advertising code
        '4' => array('showitem' => 'hidden,--palette--;;1,calltoactiontype,row_description,image,title,bodytext,--palette--,link,linktext,amount,purpose,advertising_code,-advertising_code-div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,starttime,endtime,sys_language_uid'),
        // newsletter bookmark
        '5' => array('showitem' => 'hidden,--palette--;;1,calltoactiontype,row_description,link,linktext,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,starttime,endtime,sys_language_uid'),
    ),
    'palettes' => array(
        '1' => array('showitem' => ''),
    ),
    'columns' => array(
        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => array(
                'type' => 'language',
                'renderType' => 'selectSingle',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages', -1),
                    array('LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.default_value', 0)
                ),
            ),
        ),
        'l10n_parent' => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/locallang_general.xlf:LGL.l18n_parent',
            'config' => array(
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => array(
                    array('', 0),
                ),
                'foreign_table' => 'tx_frsdrkcalltoaction_domain_model_main',
                'foreign_table_where' => 'AND tx_frsdrkcalltoaction_domain_model_main.pid=###CURRENT_PID### AND tx_frsdrkcalltoaction_domain_model_main.sys_language_uid IN (-1,0)',
            ),
        ),
        'l10n_diffsource' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),

        't3ver_label' => array(
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            )
        ),

        'hidden' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => array(
                'type' => 'check',
            ),
        ),
        'starttime' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
                'renderType' => 'inputDateTime',
                ['behaviour' => ['allowLanguageSynchronization' => true]],
            ),
        ),
        'endtime' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
                'renderType' => 'inputDateTime',
                ['behaviour' => ['allowLanguageSynchronization' => true]],
            ),
        ),
        'row_description' => array(
            'exclude' => 1,
            'label' => $langFilePrefix . 'tx_frsdrkcalltoaction_domain_model_main.row_description',
            'config' => array(
                'type' => 'text',
                'cols' => 60,
                'rows' => 5,
                'eval' => 'trim'
            ),
        ),
        'calltoactiontype' => array(
            'exclude' => 1,
            'label' => $langFilePrefix . 'tx_frsdrkcalltoaction_domain_model_main.calltoactiontype',
            'config' => array(
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => array(
                    array($langFilePrefix . 'tx_frsdrkcalltoaction_domain_model_main.calltoactiontype.1', 1),
                    array($langFilePrefix . 'tx_frsdrkcalltoaction_domain_model_main.calltoactiontype.2', 2),
                    array($langFilePrefix . 'tx_frsdrkcalltoaction_domain_model_main.calltoactiontype.3', 3),
                    array($langFilePrefix . 'tx_frsdrkcalltoaction_domain_model_main.calltoactiontype.4', 4),
                    array($langFilePrefix . 'tx_frsdrkcalltoaction_domain_model_main.calltoactiontype.5', 5),
                ),
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ),
        ),
        'title' => array(
            'exclude' => 1,
            'label' => $langFilePrefix . 'tx_frsdrkcalltoaction_domain_model_main.title',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ),
        ),
        'image' => array(
            'exclude' => 1,
            'label' => $langFilePrefix . 'tx_frsdrkcalltoaction_domain_model_main.image',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'image',
                array(
                    'appearance' => array(
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ),
                    'maxitems' => 1,
                    'overrideChildTca' => ['types' => array(
                        '0' => array(
                            'showitem' => '
							--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
                        ),
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
                            'showitem' => '
							--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
                        )
                    )]
                ),
                'png,jpg,svg'
            ),
        ),
        'bodytext' => array(
            'exclude' => 1,
            'label' => $langFilePrefix . 'tx_frsdrkcalltoaction_domain_model_main.bodytext',
            'config' => array(
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'enableRichtext' => true,
                'fieldControl' => ['fullScreenRichtext' => ['disabled' => false, 'options' => ['title' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:bodytext.W.RTE']]]
            ),
        ),
        'link' => array(
            'exclude' => 1,
            'label' => $langFilePrefix . 'tx_frsdrkcalltoaction_domain_model_main.link',
            'config' => array(
                'type' => 'input',
                'size' => 50,
                'max' => 1024,
                'eval' => 'trim',
                'softref' => 'typolink',
                'renderType' => 'inputLink',
                'fieldControl' => ['linkPopup' => ['options' => ['title' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_link_formlabel', 'blindLinkOptions' => 'mail, file, folder', 'blindLinkFields' => 'class, params']]]
            )
        ),
        'linktext' => array(
            'exclude' => 1,
            'label' => $langFilePrefix . 'tx_frsdrkcalltoaction_domain_model_main.linktext',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ),
        ),
        'amount' => array(
            'exclude' => 1,
            'label' => $langFilePrefix . 'tx_frsdrkcalltoaction_domain_model_main.amount',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'int',
            )
        ),
        'purpose' => array(
            'exclude' => 1,
            'label' => $langFilePrefix . 'tx_frsdrkcalltoaction_domain_model_main.purpose',
            'config' => array(
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_frsdrkdonations_domain_model_purpose',
                'size' => '1',
                'maxitems' => '1',
                'minitems' => '0'
            )
        ),
        'advertising_code' => array(
            'exclude' => 1,
            'label' => $langFilePrefix . 'tx_frsdrkcalltoaction_domain_model_main.advertising_code',
            'config' => array(
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_frsdrkdonations_domain_model_advertisingcode',
                'size' => '1',
                'maxitems' => '1',
                'minitems' => '0'
            )
        ),

    ),
);

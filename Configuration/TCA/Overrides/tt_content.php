<?php
/**
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2016 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 *
 *
 * Anpassungen (c) 2016 by Stefan Bublies@dt-internet.de
 * Angepasst wurde das einfuegen der Ueberschrift zur besseren Deklaration der CE-Elemente
 */

$languageFilePrefix = 'LLL:EXT:frs_drk_calltoaction/Resources/Private/Language/locallang_db.xlf:';
$frontendLanguageFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:';

// register this plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'frs_drk_calltoaction',
    'FrsDrkCalltoaction',
    'DRK Call to action',
    'EXT:frs_drk_calltoaction/Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_Drilldown.svg'
);

// add default TypoScript code
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'frs_drk_calltoaction',
    'Configuration/TypoScript',
    'CallToAction'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
    'tx_frsdrkcalltoaction_domain_model_main',
    $languageFilePrefix
);

// configure tt_content display in BE
// Configure the default backend fields for the content element "Stage image"
$GLOBALS['TCA']['tt_content']['types']['frs_drk_calltoaction'] = array(
    'showitem' => '
        --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
        header;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_formlabel,
       --linebreak--,
        tx_frsdrkcalltoaction_main;' . $frontendLanguageFilePrefix . 'header_link_formlabel,
        rowDescription'
);

// configure database field to inline relation
$tempColumnsTtContent = array(
    'tx_frsdrkcalltoaction_main' => array(
        'exclude' => 1,
        'label' => $languageFilePrefix . 'calltoaction.label',
        'config' => array(
            'type' => 'group',
            'eval' => 'int',
            'default' => 0,
            'internal_type' => 'db',
            'allowed' => 'tx_frsdrkcalltoaction_domain_model_main',
            'size' => '1',
            'maxitems' => '1',
            'minitems' => '0',
            'show_thumbs' => '1',
            'wizards' => array(
                'suggest' => array(
                    'type' => 'suggest'
                )
            )
        ),
    ),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'tt_content',
    $tempColumnsTtContent
);
